using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MultiplayerManager : MonoBehaviour {

	
	public static MultiplayerManager instance;
	public string PlayerName;
	string MatchName="";
	string MatchPassword="";
	int MatchMaxUsers=32;
	
	public List<MPPlayer> PlayerList= new List<MPPlayer>();
	public List<MapSettings> MapList= new List<MapSettings>();
	
	public MapSettings CurrentMap = null;
	public int OldPrefix;
	public bool MatchStarted = false;
	
	void Start ()
	{
		instance = this;
		PlayerName=PlayerPrefs.GetString("PlayerName");
		CurrentMap = MapList[0];
	}
	
	void FixedUpdate() {
		instance = this;
	}
	
	public void StartServer(string servername,string serverpassword,int maxusers)
	{
		MatchName=servername;
		MatchPassword=serverpassword;
		MatchMaxUsers=maxusers;
		Network.InitializeServer(MatchMaxUsers,2550,false);
		MasterServer.RegisterHost("DeathMatch",MatchName,"");
		
	}
	void OnServerInitialized()
	{
		Server_PlayerJoinRequest(PlayerName,Network.player);
		
	}
	void OnConnectedToServer()
	{
		networkView.RPC("Server_PlayerJoinRequest",RPCMode.Server,PlayerName,Network.player);
	}
	void OnPlayerDisconnected(NetworkPlayer id)
	{
		networkView.RPC("Client_RemovePlayer",RPCMode.All,id);
	}
	void OnPlayerConnected(NetworkPlayer player)
	{
		foreach(MPPlayer pl in PlayerList) {
			networkView.RPC("Client_AddPlayerToList", player, pl.PlayerName, pl.PlayerNetwork);
		}
		
		networkView.RPC("Client_GetMultiplayerMatchSettings", player, CurrentMap.MapName, "", "");
	}
	
	void OnDisconnectedFromServer() {
		PlayerList.Clear();
	}
	
	[RPC]
	void Server_PlayerJoinRequest(string playername,NetworkPlayer view)
	{
		networkView.RPC("Client_AddPlayerToList",RPCMode.All,playername,view);
	}
	[RPC]
	void Client_AddPlayerToList (string playername,NetworkPlayer view)
	{
		MPPlayer tempplayer = new MPPlayer();
		tempplayer.PlayerName = playername;
		tempplayer.PlayerNetwork = view;
		PlayerList.Add(tempplayer);

	}
	[RPC]
	void Client_RemovePlayer (NetworkPlayer view)
	{
		MPPlayer temppl=null;
		foreach (MPPlayer pl in PlayerList)
		{
			if (pl.PlayerNetwork==view)
				temppl=pl;
			if (temppl != null)
				PlayerList.Remove(temppl);
		}
	}
	[RPC]
	void Client_GetMultiplayerMatchSettings(string map, string mode, string others) {
		CurrentMap = GetMap (map);
	}
	[RPC]
	void Client_LoadMultiplayerMap(string map, int prefix) {
		Network.SetLevelPrefix(prefix);
		Application.LoadLevel(map);
	}
	
	
	public MapSettings GetMap(string name) {
		MapSettings get = null;
		
		foreach(MapSettings map in MapList) {
			if(map.MapName == name) {
				get = map;
				break;
			}
		}
		
		return get;
	}
}

[System.Serializable]
public class MPPlayer 
{
	public string PlayerName="";
	public NetworkPlayer PlayerNetwork;
	
	
	
	}

[System.Serializable]
public class MapSettings {
	public string MapName, MapLoadName;
	public Texture MapLoadTexture;
}