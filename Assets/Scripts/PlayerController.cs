using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	// Holder for Weapons
	public Transform WalkAnimationHolder;
	public Transform JumpAnimationHolder;
	public Transform SwayHolder;
	public Transform RecoilHolder;
	
	public void FixedUpdate() {
		AnimationController();
		SwayController();
	}
	
	public void AnimationController() {
		
	}
	
	public void SwayController() {
		
	}
}
