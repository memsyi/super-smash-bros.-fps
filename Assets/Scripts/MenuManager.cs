using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuManager : MonoBehaviour {
	public MenuManager instance;

	public string CurrentMenu;
	public string MatchName="";
	public string MatchPassword="";
	public int MatchMaxPlayers=32;
	private Vector2 ScrollLobby= Vector2.zero;
	
	
	private string ipTemp = "127.0.0.1";
	
	void Start ()
	{
		instance = this;
		CurrentMenu="Main";
		MatchName="Welcome"+Random.Range(1,100);
	}
	
	void FixedUpdate() {
		instance = this;
	}
	
	void OnGUI()
	{
		if (CurrentMenu=="Main")
			Menu_Main();
		if (CurrentMenu=="Lobby")
			Menu_Lobby();
		if (CurrentMenu=="Host")
			Menu_HostGame();
		if(CurrentMenu == "ChooseMap")
			Menu_ChooseMap();
		
	}
	public void NavigateTo(string nextmenu)
	{
		CurrentMenu= nextmenu;
	}
	
	private void Menu_Main()
	{
		if(GUI.Button(new Rect(10,10,100,50), "Host Game")) {
			NavigateTo("Host");
		}
		
		if(GUI.Button(new Rect(500,10,100,50), "Refresh")) {
			MasterServer.RequestHostList("DeathMatch");
		}
		
		GUI.Label(new Rect(130,25,100,30), "Player Name");
		MultiplayerManager.instance.PlayerName = GUI.TextField(new Rect(260,25,100,25), MultiplayerManager.instance.PlayerName);
		if(GUI.Button(new Rect(370,10,100,50), "Save Name")) {
			PlayerPrefs.SetString("PlayerName", MultiplayerManager.instance.PlayerName);
		}
		
		GUI.Label(new Rect(130,50,130,30), "Direct Connect");
		ipTemp = GUI.TextField(new Rect(260,50,150,25), ipTemp);
		if(GUI.Button(new Rect(370,50,100,50), "Connect")) {
			Network.Connect(ipTemp, 2550);
		}
		
		GUILayout.BeginArea(new Rect(Screen.width-400,0,400,Screen.height),"Server List","Box");
		foreach (HostData match in MasterServer.PollHostList())
		{
			GUILayout.BeginHorizontal("Box");
			
			GUILayout.Label(match.gameName);
			if (GUILayout.Button("Connect"))
			{
				Network.Connect(match);
			}
			GUILayout.EndHorizontal();
	
		}
		
		GUILayout.EndArea();
	}
	private void Menu_HostGame()
	{
		if (GUI.Button(new Rect(10,10,100,50),"Back"))
		{
		NavigateTo("Main");
		}
		if (GUI.Button(new Rect(10,60,100,50),"Start Server"))
		{
		MultiplayerManager.instance.StartServer(MatchName,MatchPassword,MatchMaxPlayers);
		}
		
		if(GUI.Button(new Rect(10, 160, 100, 50), "Choose Map")) {
			NavigateTo("ChooseMap");
		}
		
		GUI.Label(new Rect(130,25,100,30),"Match Name");
		MatchName = GUI.TextField(new Rect(260,25,100,25),MatchName);
		
		GUI.Label(new Rect(130,50,100,30),"Match Password");
		MatchPassword = GUI.PasswordField(new Rect(260,50,100,25),MatchPassword,'*');
		
		GUI.Label(new Rect(130,75,100,50),"Match Max Players");
		GUI.TextField(new Rect(260,75,50,25), (MatchMaxPlayers + 1).ToString());
		
		MatchMaxPlayers=Mathf.Clamp(MatchMaxPlayers,8,31);
		if (GUI.Button(new Rect(310,75,25,25),"+")){MatchMaxPlayers+=2;}
		if (GUI.Button(new Rect(335,75,25,25),"-")){MatchMaxPlayers-=2;}
		
		GUI.Label(new Rect(650, 10, 130, 30), MultiplayerManager.instance.CurrentMap.MapName);
	}
	private void Menu_Lobby()
	{
		GUILayout.BeginScrollView(ScrollLobby,GUILayout.MaxWidth(200));
		
		foreach(MPPlayer pl in MultiplayerManager.instance.PlayerList)
		{
			if(pl.PlayerNetwork == Network.player)
				GUI.color = Color.blue;
			GUILayout.Box(pl.PlayerName);
			GUI.color = Color.white;
		}
		
		GUILayout.EndScrollView();
		
		
		
		GUI.Box(new Rect(250, 10, 200, 40), MultiplayerManager.instance.CurrentMap.MapName);
		
		if(Network.isServer && GUI.Button(new Rect(Screen.width - 200, Screen.height - 80, 200, 30), "Start Match")) {
			MultiplayerManager.instance.networkView.RPC("Client_LoadMultiplayerMap", RPCMode.All, MultiplayerManager.instance.CurrentMap.MapLoadName, MultiplayerManager.instance.OldPrefix + 1);
			MultiplayerManager.instance.OldPrefix += 1;
			MultiplayerManager.instance.MatchStarted = true;
		}
		if(GUI.Button(new Rect(Screen.width - 200, Screen.height - 40, 200, 30), "Disconnect")) {
			Network.Disconnect();
		}
	}
	private void Menu_ChooseMap() {
		if(GUI.Button(new Rect(10, 10, 200, 50), "Back")) {
			NavigateTo("Host");
		}
		
		GUI.Label(new Rect(220, 10, 130, 30), "Choose Map");
		GUILayout.BeginArea(new Rect(350, 10, 150, Screen.height));
		
		foreach(MapSettings map in MultiplayerManager.instance.MapList) {
			if(GUILayout.Button(map.MapName)) {
				NavigateTo("Host");
				MultiplayerManager.instance.CurrentMap = map;
			}
		}
		
		GUILayout.EndArea();
	}
	void OnServerInitialized()
	{
		NavigateTo("Lobby");
	}
	void OnConnectedToServer()
	{
		NavigateTo("Lobby");
	}
	void OnDisconnectedFromServer(NetworkDisconnection info) {
		NavigateTo("Main");
	}
}

